set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}")
include(build)

buildVEZ()

add_library(Dependencies::VEZ INTERFACE IMPORTED GLOBAL)
target_include_directories(Dependencies::VEZ INTERFACE ${CMAKE_CURRENT_BINARY_DIR}/v-ez/Source)
set_property(TARGET Dependencies::VEZ PROPERTY INTERFACE_LINK_DIRECTORIES ${CMAKE_CURRENT_BINARY_DIR}/v-ez/Bin/x86_64/)
set_property(TARGET Dependencies::VEZ PROPERTY INTERFACE_LINK_LIBRARIES VEZ)

if (MSVC)# AND CMAKE_BUILD_TYPE STREQUAL "Debug")
  set_property(TARGET Dependencies::VEZ PROPERTY INTERFACE_LINK_LIBRARIES VEZd)
endif()
