#ifndef __E_BLEND_FACTOR_H_INCLUDED__
#define __E_BLEND_FACTOR_H_INCLUDED__

namespace saga
{
namespace video
{

enum E_BLEND_FACTOR
{
  ZERO	= 0,			//!< src & dest	(0, 0, 0, 0)
  ONE,					//!< src & dest	(1, 1, 1, 1)
  DST_COLOR, 				//!< src	(destR, destG, destB, destA)
  ONE_MINUS_DST_COLOR,	//!< src	(1-destR, 1-destG, 1-destB, 1-destA)
  SRC_COLOR,				//!< dest	(srcR, srcG, srcB, srcA)
  ONE_MINUS_SRC_COLOR, 	//!< dest	(1-srcR, 1-srcG, 1-srcB, 1-srcA)
  SRC_ALPHA,				//!< src & dest	(srcA, srcA, srcA, srcA)
  ONE_MINUS_SRC_ALPHA,	//!< src & dest	(1-srcA, 1-srcA, 1-srcA, 1-srcA)
  DST_ALPHA,				//!< src & dest	(destA, destA, destA, destA)
  ONE_MINUS_DST_ALPHA,	//!< src & dest	(1-destA, 1-destA, 1-destA, 1-destA)
  SRC_ALPHA_SATURATE		//!< src	(min(srcA, 1-destA), idem, ...)
};

} // namespace scene
} // namespace saga

#endif

