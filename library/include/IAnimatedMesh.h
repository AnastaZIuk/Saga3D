// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_ANIMATED_MESH_H_INCLUDED__
#define __I_ANIMATED_MESH_H_INCLUDED__

#include "aabbox3d.h"
#include "IMesh.h"

namespace saga
{
namespace scene
{
  //! Interface for an animated mesh.
  /** There are already simple implementations of this interface available so
  you don't have to implement this interface on your own if you need to:
  You might want to use saga::scene::SAnimatedMesh, saga::scene::SMesh,
  saga::scene::SMeshBuffer etc. */
  class IAnimatedMesh : public IMesh
  {
  public:
    //! Gets the frame count of the animated mesh.
    /** Note that the play-time is usually getFrameCount()-1 as it stops as soon as the last frame-key is reached.
    \return The amount of frames. If the amount is 1,
    it is a static, non animated mesh.   */
    virtual std::uint32_t getFrameCount() const = 0;

    //! Gets the animation speed of the animated mesh.
    /** \return The number of frames per second to play the
    animation with by default. If the amount is 0,
    it is a static, non animated mesh. */
    virtual float getAnimationSpeed() const = 0;

    //! Sets the animation speed of the animated mesh.
    /** \param fps Number of frames per second to play the
    animation with by default. If the amount is 0,
    it is not animated. The actual speed is set in the
    scene node the mesh is instantiated in.*/
    virtual void setAnimationSpeed(float fps) = 0;

    //! Set active animation
    /** \param index: index of the animation */
    virtual void setAnimation(const uint32_t index) = 0;

    //! Animate the mesh
    /** Depends on underlying implementation, skeletal for mesh animation will be performed */
    virtual void onAnimate(const float time) = 0;

  };

} // namespace scene
} // namespace saga

#endif

