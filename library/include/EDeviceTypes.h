// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __E_DEVICE_TYPES_H_INCLUDED__
#define __E_DEVICE_TYPES_H_INCLUDED__

namespace saga
{

  //! An enum class for the different device types supported by the Irrlicht Engine.
  enum class E_DEVICE_TYPE
  {
    SDL,
  };

} // namespace saga

#endif // __E_DEVICE_TYPES_H_INCLUDED__

