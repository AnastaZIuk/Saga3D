// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __S_ANIMATED_MESH_H_INCLUDED__
#define __S_ANIMATED_MESH_H_INCLUDED__

#include "IAnimatedMesh.h"
#include "IMesh.h"
#include "aabbox3d.h"

namespace saga
{
namespace scene
{

  //! Simple implementation of the IAnimatedMesh interface.
  struct SAnimatedMesh : public IAnimatedMesh
  {
    //! constructor
    SAnimatedMesh(const std::shared_ptr<scene::IMesh>& mesh = nullptr) : IAnimatedMesh(), FramesPerSecond(25.f)
    {
      addMesh(mesh);
      recalculateBoundingBox();
    }

    //! destructor
    virtual ~SAnimatedMesh()
    {
      Meshes.clear();
    }

    virtual E_MESH_TYPE getMeshType() const override { return E_MESH_TYPE::ANIMATED; }

    //! Gets the frame count of the animated mesh.
    /** \return Amount of frames. If the amount is 1, it is a static, non animated mesh. */
    virtual std::uint32_t getFrameCount() const
    {
      return Meshes.size();
    }

    //! Gets the default animation speed of the animated mesh.
    /** \return Amount of frames per second. If the amount is 0, it is a static, non animated mesh. */
    virtual float getAnimationSpeed() const
    {
      return FramesPerSecond;
    }

    //! Gets the frame count of the animated mesh.
    /** \param fps Frames per second to play the animation with. If the amount is 0, it is not animated.
    The actual speed is set in the scene node the mesh is instantiated in.*/
    virtual void setAnimationSpeed(float fps)
    {
      FramesPerSecond=fps;
    }

    //! Returns the IMesh interface for a frame.
    /** \param frame: Frame number as zero based index. The maximum frame number is
    getFrameCount() - 1;
    \param detailLevel: Level of detail. 0 is the lowest,
    255 the highest level of detail. Most meshes will ignore the detail level.
    \param startFrameLoop: start frame
    \param endFrameLoop: end frame
    \return The animated mesh based on a detail level. */
    virtual const std::shared_ptr<IMesh>& getMesh(std::int32_t frame, std::int32_t detailLevel=255, std::int32_t startFrameLoop=-1, std::int32_t endFrameLoop=-1)
    {
      return Meshes[frame];
    }

    //! adds a Mesh
    void addMesh(const std::shared_ptr<IMesh>& mesh)
    {
      if (mesh)
      {
        Meshes.push_back(mesh);
      }
    }

    //! Returns an axis aligned bounding box of the mesh.
    /** \return A bounding box of this mesh is returned. */
    virtual const core::aabbox3d<float>& getBoundingBox() const
    {
      return Box;
    }

    //! set user axis aligned bounding box
    virtual void setBoundingBox(const core::aabbox3df& box)
    {
      Box = box;
    }

    //! Recalculates the bounding box.
    void recalculateBoundingBox()
    {
      Box.reset(0,0,0);

      if (Meshes.empty())
        return;

      Box = Meshes[0]->getBoundingBox();

      for (std::uint32_t i=1; i < Meshes.size(); ++i)
        Box.addInternalBox(Meshes[i]->getBoundingBox());
    }

    //! returns amount of mesh buffers.
    virtual std::uint32_t getMeshBufferCount() const
    {
      if (Meshes.empty())
        return 0;

      return Meshes[0]->getMeshBufferCount();
    }

    //! returns pointer to a mesh buffer
    virtual IMeshBuffer& getMeshBuffer(std::uint32_t nr = 0) override
    {
      return Meshes[0]->getMeshBuffer(nr);
    }

    //! adds a MeshBuffer
    /** The bounding box is not updated automatically. */
    virtual void addMeshBuffer(std::unique_ptr<IMeshBuffer> buf) override
    {

    }

    //! Set active animation
    /** \param index: index of the animation */
    virtual void setAnimation(const uint32_t index) override
    {

    }

    //! Animate the mesh
    virtual void onAnimate(const float time) override
    {

    }

    //! All meshes defining the animated mesh
    std::vector<std::shared_ptr<IMesh>> Meshes;

    //! The bounding box of this mesh
    core::aabbox3d<float> Box;

    //! Default animation speed of this mesh.
    float FramesPerSecond;
  };


} // namespace scene
} // namespace saga

#endif

