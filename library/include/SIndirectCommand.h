#ifndef __SINDIRECT_COMMAND_H_INCLUDED__
#define __SINDIRECT_COMMAND_H_INCLUDED__

namespace saga
{
namespace video
{
  struct SIndirectCommand
  {
    std::uint32_t VertexCount = 0;
    std::uint32_t InstanceCount = 1;
    std::uint32_t VertexOffset = 0;
    std::uint32_t FirstInstance = 0;
  };

  struct SIndexedIndirectCommand
  {
    std::uint32_t IndexCount = 0;
    std::uint32_t InstanceCount = 1;
    std::uint32_t IndexOffset = 0;
    std::int32_t  VertexOffset = 0;
    std::uint32_t FirstInstance = 0;
  };

} // namespace scene
} // namespace saga

#endif // __SINDIRECT_COMMAND_H_INCLUDED__

