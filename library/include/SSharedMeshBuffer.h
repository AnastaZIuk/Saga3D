// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __S_SHARED_MESH_BUFFER_H_INCLUDED__
#define __S_SHARED_MESH_BUFFER_H_INCLUDED__


#include "IMeshBuffer.h"

namespace saga
{
namespace scene
{
  //! Implementation of the IMeshBuffer interface with shared vertex list
  struct SSharedMeshBuffer : public IMeshBuffer
  {
    //! constructor
    SSharedMeshBuffer()
      : IMeshBuffer()
      , Vertices(0), ChangedID_Vertex(1), ChangedID_Index(1)
      , MappingHintVertex(E_HARDWARE_MAPPING::NEVER), MappingHintIndex(E_HARDWARE_MAPPING::NEVER)
      , PrimitiveType(E_PRIMITIVE_TYPE::TRIANGLES)
    {
      #ifdef _DEBUG
      setDebugName("SSharedMeshBuffer");
      #endif
    }

    //! constructor
    SSharedMeshBuffer(std::vector<video::S3DVertex> *vertices) : IMeshBuffer(), Vertices(vertices), ChangedID_Vertex(1), ChangedID_Index(1), MappingHintVertex(E_HARDWARE_MAPPING::NEVER), MappingHintIndex(E_HARDWARE_MAPPING::NEVER)
    {
      #ifdef _DEBUG
      setDebugName("SSharedMeshBuffer");
      #endif
    }

    // //! returns the material of this meshbuffer
    // virtual const video::SMaterial& getMaterial() const
    // {
    //   return Material;
    // }

    // //! returns the material of this meshbuffer
    // virtual video::SMaterial& getMaterial()
    // {
    //   return Material;
    // }

    //! returns pointer to vertices
    virtual const void* getVertices() const
    {
      if (Vertices)
        return Vertices->const_pointer();
      else
        return 0;
    }

    //! returns pointer to vertices
    virtual void* getVertices()
    {
      if (Vertices)
        return Vertices->pointer();
      else
        return 0;
    }

    //! returns amount of vertices
    virtual std::size_t getVertexCount() const
    {
      if (Vertices)
        return Vertices->size();
      else
        return 0;
    }

    //! returns pointer to indices
    virtual const std::uint16_t* getIndices() const
    {
      return Indices.const_pointer();
    }

    //! returns pointer to indices
    virtual std::uint16_t* getIndices()
    {
      return Indices.pointer();
    }

    //! returns amount of indices
    virtual std::uint32_t getIndexCount() const
    {
      return Indices.size();
    }

    //! Get type of index data which is stored in this meshbuffer.
    virtual scene::E_INDEX_TYPE getIndexType() const
    {
      return scene::E_INDEX_TYPE::BITS_16;
    }

    //! returns an axis aligned bounding box
    virtual const core::aabbox3d<float>& getBoundingBox() const
    {
      return BoundingBox;
    }

    //! set user axis aligned bounding box
    virtual void setBoundingBox(const core::aabbox3df& box)
    {
      BoundingBox = box;
    }

    //! returns which type of vertex data is stored.
    virtual scene::E_VERTEX_TYPE getVertexType() const
    {
      return scene::E_VERTEX_TYPE::STANDARD;
    }

    //! recalculates the bounding box. should be called if the mesh changed.
    virtual void recalculateBoundingBox()
    {
      if (!Vertices || Vertices->empty() || Indices.empty())
        BoundingBox.reset(0,0,0);
      else
      {
        BoundingBox.reset((*Vertices)[Indices[0]].Pos);
        for (std::uint32_t i=1; i < Indices.size(); ++i)
          BoundingBox.addInternalPoint((*Vertices)[Indices[i]].Pos);
      }
    }

    //! returns position of vertex i
    virtual const glm::vec3& getPosition(std::uint32_t i) const
    {
      return (*Vertices)[Indices[i]].Pos;
    }

    //! returns position of vertex i
    virtual glm::vec3& getPosition(std::uint32_t i)
    {
      return (*Vertices)[Indices[i]].Pos;
    }

    //! returns normal of vertex i
    virtual const glm::vec3& getNormal(std::uint32_t i) const
    {
      return (*Vertices)[Indices[i]].Normal;
    }

    //! returns normal of vertex i
    virtual glm::vec3& getNormal(std::uint32_t i)
    {
      return (*Vertices)[Indices[i]].Normal;
    }

    //! returns texture coord of vertex i
    virtual const glm::vec2& getTCoords(std::uint32_t i) const
    {
      return (*Vertices)[Indices[i]].TCoords;
    }

    //! returns texture coord of vertex i
    virtual glm::vec2& getTCoords(std::uint32_t i)
    {
      return (*Vertices)[Indices[i]].TCoords;
    }

    //! append the vertices and indices to the current buffer
    virtual void append(const void* const vertices, std::uint32_t numVertices, const std::uint16_t* const indices, std::uint32_t numIndices) {}

    //! append the meshbuffer to the current buffer
    virtual void append(const IMeshBuffer* const other) {}

    //! get the current hardware mapping hint
    virtual E_HARDWARE_MAPPING getHardwareMappingHint_Vertex() const
    {
      return MappingHintVertex;
    }

    //! get the current hardware mapping hint
    virtual E_HARDWARE_MAPPING getHardwareMappingHint_Index() const
    {
      return MappingHintIndex;
    }

    //! set the hardware mapping hint, for driver
    virtual void setHardwareMappingHint(E_HARDWARE_MAPPING NewMappingHint, E_BUFFER_TYPE buffer=E_BUFFER_TYPE::VERTEX_AND_INDEX)
    {
      if (buffer==E_BUFFER_TYPE::VERTEX_AND_INDEX || buffer==E_BUFFER_TYPE::VERTEX)
        MappingHintVertex=NewMappingHint;
      if (buffer==E_BUFFER_TYPE::VERTEX_AND_INDEX || buffer==E_BUFFER_TYPE::INDEX)
        MappingHintIndex=NewMappingHint;
    }

    //! Describe what kind of primitive geometry is used by the meshbuffer
    virtual void setPrimitiveType(scene::E_PRIMITIVE_TYPE type)
    {
      PrimitiveType = type;
    }

    //! Get the kind of primitive geometry which is used by the meshbuffer
    virtual scene::E_PRIMITIVE_TYPE getPrimitiveType() const
    {
      return PrimitiveType;
    }

    //! flags the mesh as changed, reloads hardware buffers
    virtual void setDirty(E_BUFFER_TYPE buffer=E_BUFFER_TYPE::VERTEX_AND_INDEX)
    {
      if (buffer==E_BUFFER_TYPE::VERTEX_AND_INDEX || buffer==E_BUFFER_TYPE::VERTEX)
        ++ChangedID_Vertex;
      if (buffer==E_BUFFER_TYPE::VERTEX_AND_INDEX || buffer==E_BUFFER_TYPE::INDEX)
        ++ChangedID_Index;
    }

    //! Get the currently used ID for identification of changes.
    /** This shouldn't be used for anything outside the VideoDriver. */
    virtual std::uint32_t getChangedID_Vertex() const {return ChangedID_Vertex;}

    //! Get the currently used ID for identification of changes.
    /** This shouldn't be used for anything outside the VideoDriver. */
    virtual std::uint32_t getChangedID_Index() const {return ChangedID_Index;}

    //! Material of this meshBuffer
    //video::SMaterial Material;

    //! Shared Array of vertices
    std::vector<video::S3DVertex> *Vertices;

    //! Array of indices
    std::vector<std::uint16_t> Indices;

    //! ID used for hardware buffer management
    std::uint32_t ChangedID_Vertex;

    //! ID used for hardware buffer management
    std::uint32_t ChangedID_Index;

    //! Bounding box
    core::aabbox3df BoundingBox;

    //! hardware mapping hint
    E_HARDWARE_MAPPING MappingHintVertex;
    E_HARDWARE_MAPPING MappingHintIndex;

    //! Primitive type used for rendering (triangles, lines, ...)
    scene::E_PRIMITIVE_TYPE PrimitiveType;
  };


} // namespace scene
} // namespace saga

#endif

