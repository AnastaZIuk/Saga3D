#ifndef __SPUSH_CONSTANT_H_INCLUDED__
#define __SPUSH_CONSTANT_H_INCLUDED__

#include "SGPUResource.h"
#include <memory>

namespace saga
{
namespace video
{
  struct SPushConstant : public SGPUResource
  {
    std::unique_ptr<void*> Data;
    std::size_t Size;
  };

  using PushConstantHandle = SGPUResource::HandleType;

} // namespace scene
} // namespace saga

#endif // __SPUSH_CONSTANT_H_INCLUDED__

