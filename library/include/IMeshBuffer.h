// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_MESH_BUFFER_H_INCLUDED__
#define __I_MESH_BUFFER_H_INCLUDED__

#include "aabbox3d.h"
#include "S3DVertex.h"
#include "EPrimitiveTypes.h"
#include "SPipeline.h"
#include "SIndirectBuffer.h"
#include <vector>

namespace saga
{

namespace video
{
  class IVideoDriver;
}

namespace scene
{
  using MeshBufferID = std::uint32_t;

  //! Struct for holding a mesh with a single material.
  /** A part of an IMesh which has the same material on each face of that
  group. Logical groups of an IMesh need not be put into separate mesh
  buffers, but can be. Separately animated parts of the mesh must be put
  into separate mesh buffers.
  Some mesh buffer implementations have limitations on the number of
  vertices the buffer can hold. In that case, logical grouping can help.
  Moreover, the number of vertices should be optimized for the GPU upload,
  which often depends on the type of gfx card. Typical figures are
  1000-10000 vertices per buffer.
  SMeshBuffer is a simple implementation of a MeshBuffer, which supports
  up to 65535 vertices.

  Since meshbuffers are used for drawing, and hence will be exposed
  to the driver, chances are high that they are grab()'ed from somewhere.
  It's therefore required to dynamically allocate meshbuffers which are
  passed to a video driver and only drop the buffer once it's not used in
  the current code block anymore.
  */
  class IMeshBuffer
  {
  public:
    virtual ~IMeshBuffer() {}

    virtual bool isGPUBuffer() const = 0;

    //! Returns internal ID to identify mesh buffer
    /** Each mesh buffer will be assigned an unique ID in its creation */
    virtual std::uint64_t getID() const = 0;

    //! Generate GPU-compatible data
    /** Depending on render pass's pipeline layout, this function generates vertex data for that pass */
    virtual void buildBuffer(video::IVideoDriver& driver, const video::PipelineHandle pipeline) = 0;

    //! Get pointer to GPU staging buffer
    /** \return Pointer to staging buffer. */
    virtual const void* getData(const video::PipelineHandle pipeline) const = 0;

    //! Get size of GPU staging buffer
    /** \return Size of staging buffer. */
    virtual std::size_t getSize(const video::PipelineHandle pipeline) const = 0;

    //! Get access to vertex data. The data is an array of vertices.
    /** Which vertex type is used can be determined by getVertexType().
    \return Pointer to array of vertices. */
    virtual void* getVertices() = 0;
    
    //! Get amount of vertices in meshbuffer.
    /** \return Number of vertices in this buffer. */
    virtual std::size_t getVertexCount() const = 0;

    //! Get access to indices.
    /** \return Pointer to indices array. */
    virtual const std::uint32_t* getIndices() const = 0;

    //! Get access to indices.
    /** \return Pointer to indices array. */
    virtual std::uint32_t* getIndices() = 0;

    //! Get amount of indices in this meshbuffer.
    /** \return Number of indices in this buffer. */
    virtual std::uint32_t getIndexCount() const = 0;

    //! Get the axis aligned bounding box of this meshbuffer.
    /** \return Axis aligned bounding box of this buffer. */
    virtual const core::aabbox3df& getBoundingBox() const = 0;

    //! Set axis aligned bounding box
    /** \param box User defined axis aligned bounding box to use
    for this buffer. */
    virtual void setBoundingBox(const core::aabbox3df& box) = 0;

    //! Recalculates the bounding box. Should be called if the mesh changed.
    virtual void recalculateBoundingBox() = 0;

    //! returns position of vertex i
    virtual const glm::vec3& getPosition(std::uint32_t i) const = 0;

    //! returns normal of vertex i
    virtual const glm::vec3& getNormal(std::uint32_t i) const = 0;

    //! returns texture coord of vertex i
    virtual const glm::vec2& getTCoords(std::uint32_t i) const = 0;

    //! returns tangent of vertex i
    virtual const glm::vec3& getTangent(std::uint32_t i) const = 0;

    //! returns bi-tangent of vertex i
    virtual const glm::vec3& getBiTangent(std::uint32_t i) const = 0;

    //! returns vertex index at this offset
    virtual std::uint32_t getIndex(std::size_t offset) const = 0;

    //! Append the vertices and indices to the current buffer
    /** Only works for compatible vertex types.
    \param vertices vertex vector.
    \param indices index vector. */
    virtual void append(std::vector<S3DVertex>&& vertices, std::vector<uint32_t>&& indices) = 0;

    //! Append the meshbuffer to the current buffer
    /** Only works for compatible vertex types
    \param other Buffer to append to this one. */
    virtual void append(const IMeshBuffer* const other) = 0;

    //! Append the custom attribute buffer to the current mesh buffer
    /**
    \param buffer Pointer to the buffer containing data
    \param size Total size of buffer (bytes)
    \param stride The custom data size of each vertex (bytes) */
    virtual void appendAttribute(const char* buffer, const size_t size, const size_t stride) = 0;

    //! Describe what kind of primitive geometry is used by the meshbuffer
    /** Note: Default is E_PRIMITIVE_TYPE::TRIANGLES. Using other types is fine for rendering.
    But meshbuffer manipulation functions might expect type E_PRIMITIVE_TYPE::TRIANGLES
    to work correctly. Also mesh writers will generally fail (badly!) with other
    types than E_PRIMITIVE_TYPE::TRIANGLES. */
    void setPrimitiveType(scene::E_PRIMITIVE_TYPE type)
    {
      PrimitiveType = type;
    }

    //! Get the kind of primitive geometry which is used by the meshbuffer
    scene::E_PRIMITIVE_TYPE getPrimitiveType() const
    {
      return PrimitiveType;
    }

    //! Set buffer binding location
    void setInstanceCount(const size_t count)
    {
      InstanceCount = count;
    }

    //! Get buffer binding location
    std::size_t getInstanceCount() const
    {
      return InstanceCount;
    }

    //! Set buffer binding location
    void setBindingLocation(const int binding)
    {
      Binding = binding;
    }

    //! Get buffer binding location
    int getBindingLocation() const
    {
      return Binding;
    }

    void isNull(const bool null)
    {
      IsNull = null;
    }

    bool isNull() const
    {
      return IsNull;
    }

    //! Calculate how many geometric primitives are used by this meshbuffer
    virtual std::uint32_t getPrimitiveCount() const
    {
      std::uint32_t indexCount = getIndexCount();
      switch (getPrimitiveType())
      {
        case scene::E_PRIMITIVE_TYPE::POINTS:         return indexCount;
        case scene::E_PRIMITIVE_TYPE::LINE_STRIP:     return indexCount-1;
        case scene::E_PRIMITIVE_TYPE::LINE_LOOP:      return indexCount;
        case scene::E_PRIMITIVE_TYPE::LINES:          return indexCount/2;
        case scene::E_PRIMITIVE_TYPE::TRIANGLE_STRIP: return (indexCount-2);
        case scene::E_PRIMITIVE_TYPE::TRIANGLE_FAN:   return (indexCount-2);
        case scene::E_PRIMITIVE_TYPE::TRIANGLES:      return indexCount/3;
        case scene::E_PRIMITIVE_TYPE::QUAD_STRIP:     return (indexCount-2)/2;
        case scene::E_PRIMITIVE_TYPE::QUADS:          return indexCount/4;
        case scene::E_PRIMITIVE_TYPE::POLYGON:        return indexCount; // (not really primitives, that would be 1, works like line_strip)
        case scene::E_PRIMITIVE_TYPE::POINT_SPRITES:  return indexCount;
      }
      return 0;
    }

    bool useIndirectDraw() const
    {
      return IndirectDrawBuffer != video::NULL_GPU_RESOURCE_HANDLE;
    }

    auto indirectDrawBuffer() const
    {
      return IndirectDrawBuffer;
    }

    void setIndirectDrawBuffer(const video::IndirectBufferHandle buffer)
    {
      IndirectDrawBuffer = buffer;
    }

    bool useIndexedIndirectDraw() const
    {
      return IndexedIndirectDrawBuffer != video::NULL_GPU_RESOURCE_HANDLE;
    }

    auto indexedIndirectDrawBuffer() const
    {
      return IndexedIndirectDrawBuffer;
    }

    void setIndexedIndirectDrawBuffer(const video::IndexedIndirectBufferHandle buffer)
    {
      IndexedIndirectDrawBuffer = buffer;
    }

  protected:
    //! To set ID for mesh buffer when created
    static MeshBufferID RootID;

    //! Primitive type used for rendering (triangles, lines, ...)
    scene::E_PRIMITIVE_TYPE PrimitiveType = E_PRIMITIVE_TYPE::TRIANGLES;

    //! Instance count for rendering multiple objects with this buffer
    std::size_t InstanceCount = 1;

    //! Binding location
    int Binding = 0;

    //! Is a null buffer?
    bool IsNull = false;

    //! Indirect draw command buffer for this mesh buffer
    video::IndirectBufferHandle IndirectDrawBuffer = video::NULL_GPU_RESOURCE_HANDLE;
    
    //! Indexed indirect draw command buffer for this mesh buffer
    video::IndexedIndirectBufferHandle IndexedIndirectDrawBuffer = video::NULL_GPU_RESOURCE_HANDLE;
  };

} // namespace scene
} // namespace saga

#endif
