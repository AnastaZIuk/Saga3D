// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __E_SCENE_NODE_TYPES_H_INCLUDED__
#define __E_SCENE_NODE_TYPES_H_INCLUDED__

namespace saga
{
namespace scene
{

  //! An enumeration for all types of built-in scene nodes
  /** A scene node type is represented by a four character code
  such as 'cube' or 'mesh' instead of simple numbers, to avoid
  name clashes with external scene nodes.*/
  enum class E_SCENE_NODE_TYPE
  {
    //! simple cube scene node
    CUBE,

    //! Sphere scene node
    SPHERE,

    //! Text Scene Node
    TEXT,

    //! Billboard text scene node
    BILLBOARD_TEXT,

    //! Water Surface Scene Node
    WATER_SURFACE,

    //! Terrain Scene Node
    TERRAIN,

    //! Sky Box Scene Node
    SKY_BOX,

    //! Sky Dome Scene Node
    SKY_DOME,

    //! Shadow Volume Scene Node
    SHADOW_VOLUME,

    //! Octree Scene Node
    OCTREE,

    //! Mesh Scene Node
    MESH,

    //! Light Scene Node
    LIGHT,

    //! Empty Scene Node
    EMPTY,

    //! Dummy Transformation Scene Node
    DUMMY_TRANSFORMATION,

    //! Camera Scene Node
    CAMERA,

    //! Billboard Scene Node
    BILLBOARD,

    //! Animated Mesh Scene Node
    ANIMATED_MESH,

    //! Particle System Scene Node
    PARTICLE_SYSTEM,

    //! Volume Light Scene Node
    VOLUME_LIGHT,

    //! Maya Camera Scene Node
    /** Legacy, for loading version <= 1.4.x .irr files */
    CAMERA_MAYA,

    //! First Person Shooter Camera
    /** Legacy, for loading version <= 1.4.x .irr files */
    CAMERA_FPS,

    //! Unknown scene node
    UNKNOWN,

    //! Will match with any scene node when checking types
    ANY,
  };



} // namespace scene
} // namespace saga


#endif

