#ifndef __SINDIRECT_BUFFER_H_INCLUDED__
#define __SINDIRECT_BUFFER_H_INCLUDED__

#include "SGPUResource.h"
#include "SIndirectCommand.h"
#include <vector>

namespace saga
{
namespace video
{

  struct SIndirectBuffer : public SGPUResource
  {
    std::vector<SIndirectCommand> Commands;
    std::size_t Count = 0;
    bool ShaderBinding = false;
  };

  struct SIndexedIndirectBuffer : public SGPUResource
  {
    std::vector<SIndexedIndirectCommand> Commands;
    std::size_t Count = 0;
    bool ShaderBinding = false;
  };

  using IndirectBufferHandle = SGPUResource::HandleType;
  using IndexedIndirectBufferHandle = SGPUResource::HandleType;

} // namespace scene
} // namespace saga

#endif // __SINDIRECT_BUFFER_H_INCLUDED__

