// Copyright (C) 2009-2012 Christian Stehno
// No rights reserved: this software is in the public domain.

#include "testUtils.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

// Tests mesh transformations via mesh manipulator.
bool meshTransform(void)
{
  // Use E_DRIVER_TYPE::BURNINGSVIDEO since it is not dependent on (e.g.) OpenGL driver versions.
  IrrlichtDevice *device = createDevice(E_DRIVER_TYPE::BURNINGSVIDEO, dimension2d<u32>(160, 120), 32);
  assE_RENDER_TARGET_TYPE::log(device);
  if (!device)
    return false;

  IVideoDriver* driver = device->getVideoDriver();
  ISceneManager * smgr = device->getSceneManager();

  IMeshSceneNode* node1 = smgr->addCubeSceneNode(50);
  IAnimatedMesh* amesh = smgr->getMesh("../media/sydney.md2");
  IAnimatedMesh* smesh = smgr->getMesh("../media/ninja.b3d");
  assE_RENDER_TARGET_TYPE::log(node1 && amesh && smesh);

  bool result = false;
  if (!node1 || !amesh || !smesh)
    return false;

//  node1->setPosition(glm::vec3(-60,0,150));
  node1->setDebugDataVisible(scene::EDS_BBOX_ALL);

  IMeshSceneNode* node2 = smgr->addMeshSceneNode(amesh->getMesh(10));
  assE_RENDER_TARGET_TYPE::log(node2);

  if (!node2)
    return false;

//  node2->setPosition(glm::vec3(30,10,150));
  node2->setDebugDataVisible(scene::EDS_BBOX_ALL);
  node2->setMaterialFlag(E_MATERIAL_FLAG::LIGHTING, false);

  IMeshSceneNode* node3 = smgr->addMeshSceneNode(smesh->getMesh(10));
  assE_RENDER_TARGET_TYPE::log(node3);

  if (!node3)
    return false;

//  node3->setPosition(glm::vec3(10,0,0));
  node3->setDebugDataVisible(scene::EDS_BBOX_ALL);
  node3->setMaterialFlag(E_MATERIAL_FLAG::LIGHTING, false);

  smgr->addCameraSceneNode()->setPosition(glm::vec3(0,0,-20));

  // Just jump to the last frame since that's all we're interested in.
  device->run();
  driver->beginScene(video::E_CLEAR_BUFFER_FLAG::COLOR | video::E_CLEAR_BUFFER_FLAG::DEPTH, SColor(255, 60, 60, 60));
  smgr->drawAll();
  driver->endScene();

  glm::mat4 mat;
  mat.setTranslation(glm::vec3(-60,0,150));
  driver->getMeshManipulator()->transform(node1->getMesh(), mat);

  mat.setTranslation(glm::vec3(30,10,150));
  driver->getMeshManipulator()->transform(node2->getMesh(), mat);

  mat.setTranslation(glm::vec3(10,0,0));
  driver->getMeshManipulator()->transform(node3->getMesh(), mat);

  // Just jump to the last frame since that's all we're interested in.
  device->run();
  driver->beginScene(video::E_CLEAR_BUFFER_FLAG::COLOR | video::E_CLEAR_BUFFER_FLAG::DEPTH, SColor(255, 60, 60, 60));
  smgr->drawAll();
  driver->endScene();

  result = takeScreenshotAndCompareAgainstReference(driver, "-meshTransform.png");

  device->closeDevice();
  device->run();
  device->drop();

  return result;
}
