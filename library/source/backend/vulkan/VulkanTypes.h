#ifndef __VULKAN_TYPES_H_INCLUDED__
#define __VULKAN_TYPES_H_INCLUDED__

#include <vulkan/vulkan.h>

namespace saga
{

namespace scene
{
enum class E_PRIMITIVE_TYPE;
}

namespace video
{

enum class E_ATTRIBUTE_FORMAT;
enum class E_PIXEL_FORMAT;

namespace vk
{

VkFormat getFormat(const E_ATTRIBUTE_FORMAT format);
VkFormat getFormat(const E_PIXEL_FORMAT format);
VkPrimitiveTopology getTopology(const scene::E_PRIMITIVE_TYPE type);

} // namespace vk
} // namespace video
} // namespace saga

#endif // __VULKAN_TYPES_H_INCLUDED__
