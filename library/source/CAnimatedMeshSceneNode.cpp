// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CAnimatedMeshSceneNode.h"
#include "IVideoDriver.h"
#include "ISceneManager.h"
#include "S3DVertex.h"
#include "CSkinnedMesh.h"
#include "IMesh.h"
#include "IAnimatedMesh.h"
#include "CVideoDriver.h"

namespace saga
{
namespace scene
{

//! constructor
CAnimatedMeshSceneNode::CAnimatedMeshSceneNode(
  const std::shared_ptr<IAnimatedMesh>& mesh,
  const std::shared_ptr<ISceneNode>& parent,
  const std::shared_ptr<ISceneManager>& mgr,
  const glm::vec3& position,
  const glm::vec3& rotation,
  const glm::vec3& scale)
: IAnimatedMeshSceneNode(mesh, parent, mgr, position, rotation, scale),
  StartFrame(0), EndFrame(0), FramesPerSecond(0.025f),
  CurrentFrameNumber(0.f), LastTimeMs(0)
  // TransitionTime(0), Transiting(0.f), TransitingBlend(0.f),
  // JointMode(EJUOR_NONE), JointsUsed(false),
  // Looping(true), ReadOnlyMaterials(false), RenderFromIdentity(false),
  // LoopCallBack(0), PassCount(0), Shadow(0), MD3Special(0)
{

}

//! destructor
CAnimatedMeshSceneNode::~CAnimatedMeshSceneNode()
{
  Mesh = nullptr;

  // if (LoopCallBack)
    // LoopCallBack->drop();
}

//! Sets the current frame. From now on the animation is played from this frame.
void CAnimatedMeshSceneNode::setCurrentFrame(float frame)
{
  CurrentFrameNumber = glm::clamp(frame, (float)StartFrame, (float)EndFrame);
  // beginTransition();
}

//! Returns the currently displayed frame number.
float CAnimatedMeshSceneNode::getFrameNumber() const
{
  return CurrentFrameNumber;
}

void CAnimatedMeshSceneNode::onAnimate(const float time)
{
  if (LastTimeMs== 0)  // first frame
  {
    LastTimeMs = time * 1000.f;
  }

  LastTimeMs = time * 1000.f;

  auto mesh = std::dynamic_pointer_cast<IAnimatedMesh>(Mesh);
  mesh->onAnimate(time);
  ISceneNode::onAnimate(time);
}

//! Returns the current start frame number.
std::int32_t CAnimatedMeshSceneNode::getStartFrame() const
{
  return StartFrame;
}

//! Returns the current start frame number.
std::int32_t CAnimatedMeshSceneNode::getEndFrame() const
{
  return EndFrame;
}

//! sets the frames between the animation is looped.
//! the default is 0 - MaximalFrameCount of the mesh.
bool CAnimatedMeshSceneNode::setFrameLoop(std::int32_t begin, std::int32_t end)
{
  return false;
}

//! sets the speed with witch the animation is played
void CAnimatedMeshSceneNode::setAnimationSpeed(float framesPerSecond)
{
  FramesPerSecond = framesPerSecond * 0.001f;
}

float CAnimatedMeshSceneNode::getAnimationSpeed() const
{
  return FramesPerSecond * 1000.f;
}

//! returns the axis aligned bounding box of this node
const core::aabbox3d<float>& CAnimatedMeshSceneNode::getBoundingBox() const
{
  return Box;
}

//! Gets joint count.
std::uint32_t CAnimatedMeshSceneNode::getBoneCount() const
{
  // if (!Mesh || Mesh->getMeshType() != E_ANIMATED_MESH_TYPE::SKINNED)
  //   return 0;

  // auto skinnedMesh = std::dynamic_pointer_cast<ISkinnedMesh>(Mesh);

  // return skinnedMesh->getBoneCount();

  return 0;
}

//! Sets looping mode which is on by default. If set to false,
//! animations will not be looped.
void CAnimatedMeshSceneNode::setLoopMode(bool playAnimationLooped)
{
  // Looping = playAnimationLooped;
}

//! returns the current loop mode
bool CAnimatedMeshSceneNode::getLoopMode() const
{
  // return Looping;
  return false;
}

//! Sets a callback interface which will be called if an animation
//! playback has ended. Set this to 0 to disable the callback again.
void CAnimatedMeshSceneNode::setAnimationEndCallback(IAnimationEndCallBack* callback)
{
  // if (callback == LoopCallBack)
  //   return;

  // if (LoopCallBack)
  //   LoopCallBack->drop();

  // LoopCallBack = callback;

  // if (LoopCallBack)
  //   LoopCallBack->grab();
}

//! updates the absolute position based on the relative and the parents position
void CAnimatedMeshSceneNode::updateAbsolutePosition()
{
  IAnimatedMeshSceneNode::updateAbsolutePosition();
}

//! Sets the transition time in seconds (note: This needs to enable joints, and setJointmode maybe set to 2)
//! you must call animateJoints(), or the mesh will not animate
// void CAnimatedMeshSceneNode::setTransitionTime(float time)
// {
//   const std::uint32_t ttime = (std::uint32_t)core::floor32(time*1000.0f);
//   if (TransitionTime==ttime)
//     return;
//   TransitionTime = ttime;
//   if (ttime != 0)
//     setJointMode(EJUOR_CONTROL);
//   else
//     setJointMode(EJUOR_NONE);
// }

} // namespace scene
} // namespace saga
