// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_OCTREE_TRIANGLE_SELECTOR_H_INCLUDED__
#define __C_OCTREE_TRIANGLE_SELECTOR_H_INCLUDED__

#include "CTriangleSelector.h"

namespace saga
{
namespace scene
{

class ISceneNode;

//! Stupid triangle selector without optimization
class COctreeTriangleSelector : public CTriangleSelector
{
public:

  //! Constructs a selector based on a mesh
  COctreeTriangleSelector(const IMesh* mesh, ISceneNode* node, std::int32_t minimalPolysPerNode);

  //! Constructs a selector based on a meshbuffer
  COctreeTriangleSelector(const IMeshBuffer* meshBuffer, std::uint32_t materialIndex, ISceneNode* node, std::int32_t minimalPolysPerNode);

  virtual ~COctreeTriangleSelector();

  //! Gets all triangles which lie within a specific bounding box.
  virtual void getTriangles(core::triangle3df* triangles, std::int32_t arraySize, std::int32_t& outTriangleCount,
    const core::aabbox3d<float>& box, const glm::mat4* transform, bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const override;

  //! Gets all triangles which have or may have contact with a 3d line.
  virtual void getTriangles(core::triangle3df* triangles, std::int32_t arraySize,
    std::int32_t& outTriangleCount, const core::line3d<float>& line,
    const glm::mat4* transform, bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const override;

private:

  struct SOctreeNode
  {
    SOctreeNode()
    {
      for (std::uint32_t i = 0; i!=8; ++i)
        Child[i] = 0;
    }

    ~SOctreeNode()
    {
      for (std::uint32_t i = 0; i!=8; ++i)
        delete Child[i];
    }

    std::vector<core::triangle3df> Triangles;
    SOctreeNode* Child[8];
    core::aabbox3d<float> Box;
  };


  void constructOctree(SOctreeNode* node);
  void deleteEmptyNodes(SOctreeNode* node);
  void getTrianglesFromOctree(SOctreeNode* node, std::int32_t& trianglesWritten,
      std::int32_t maximumSize, const core::aabbox3d<float>& box,
      const glm::mat4* transform,
      core::triangle3df* triangles) const;

  void getTrianglesFromOctree(SOctreeNode* node, std::int32_t& trianglesWritten,
      std::int32_t maximumSize, const core::line3d<float>& line,
      const glm::mat4* transform,
      core::triangle3df* triangles) const;

  SOctreeNode* Root;
  std::int32_t NodeCount;
  std::int32_t MinimalPolysPerNode;
};

} // namespace scene
} // namespace saga


#endif

