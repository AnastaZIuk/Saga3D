// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CIrrDeviceSDL.h"
//#include "IVideoDriver.h"
#include "IEventReceiver.h"
#include "SIrrCreationParameters.h"
#include "CVulkanDriver.h"
#include <SDL2/SDL_syswm.h>
#include <SDL2/SDL_video.h>
#include <string>

static int SDLDeviceInstances = 0;

namespace saga
{

//! constructor
CIrrDeviceSDL::CIrrDeviceSDL(const SIrrlichtCreationParameters& param)
:  CIrrDeviceStub(param),
   Screen((SDL_Surface*) param.WindowId), SDL_Flags(SDL_WINDOW_VULKAN),
   Width(param.WindowSize.x), Height(param.WindowSize.y), AspectRatio(1.f),
   Resizable(false), WindowHasFocus(false), WindowMinimized(false)
{
  if (++SDLDeviceInstances == 1)
  {
    #ifdef NDEBUG
    SDL_LogSetAllPriority(SDL_LOG_PRIORITY_INFO);
    #else
    SDL_LogSetAllPriority(SDL_LOG_PRIORITY_VERBOSE);
    #endif
    
    #ifdef SAGA_NO_LOG
    SDL_LogSetAllPriority(SDL_NUM_LOG_PRIORITIES);
    #endif

    // Initialize SDL 2
    if (CreationParams.DriverType == video::E_DRIVER_TYPE::VULKAN_HEADLESS)
    {
      SDL_SetMainReady();
    }
    if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0)
    {
      SDL_LogCritical(SDL_LOG_CATEGORY_SYSTEM, "Unable to initialize SDL: %s", SDL_GetError());
      Close = true;
    }
    else
    {
      SDL_LogDebug(SDL_LOG_CATEGORY_SYSTEM, "SDL initialized");
      SDL_LogInfo(SDL_LOG_CATEGORY_SYSTEM, "Basic systems initialized");
    }
  }

  SDL_VERSION(&Info.version);

  SDL_GetVersion(&Info.version);
  std::string sdlversion = "SDL version ";
  sdlversion += std::to_string(Info.version.major);
  sdlversion += ".";
  sdlversion += std::to_string(Info.version.minor);
  sdlversion += ".";
  sdlversion += std::to_string(Info.version.patch);
  SDL_LogDebug(SDL_LOG_CATEGORY_SYSTEM, sdlversion.c_str());

  // create window
  if (CreationParams.Fullscreen)
  {
    SDL_Flags |= SDL_WINDOW_FULLSCREEN;
  }

  /* TODO(manh): don't force render size, use device size for something else
  #if defined __ANDROID__ || TARGET_OS_IPHONE == 1
  CreationParams.Fullscreen = true;
  SDL_Flags |= SDL_WINDOW_FULLSCREEN;
  SDL_DisplayMode DM;
  SDL_GetCurrentDisplayMode(0, &DM);
  Width = DM.w;
  Height = DM.h;
  #endif
  */

  AspectRatio = float(Width) / float(Height);

  if (CreationParams.DriverType == video::E_DRIVER_TYPE::VULKAN)
  {
    createWindow();
  }

  // create driver
  createDriver();
  createSceneManager();
  VideoDriver->setSceneManager(SceneManager);
  addEventReceiver(SceneManager.get());
}

//! destructor
CIrrDeviceSDL::~CIrrDeviceSDL()
{
  if (--SDLDeviceInstances == 0)
  {
    SDL_Quit();
    SDL_LogInfo(SDL_LOG_CATEGORY_SYSTEM, "Quit SDL");
  }
}

bool CIrrDeviceSDL::createWindow()
{
  if (Close)
    return false;

  Window = SDL_CreateWindow(
    "Saga3D",
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    Width,
    Height,
    SDL_Flags
 );
  return true;
}

//! create the driver
void CIrrDeviceSDL::createDriver()
{
  auto driver = std::make_unique<video::CVulkanDriver>(CreationParams, *this);
  if (!driver->initDriver())
  {
    SDL_LogCritical(SDL_LOG_CATEGORY_RENDER, "Failed to initialize Vulkan driver");
    Close = true;
  }
  else
  {
    VideoDriver = std::move(driver);
  }
}

//! runs the device. Returns false if device wants to be deleted
bool CIrrDeviceSDL::run()
{
  SDL_Event SDL_event;

  while (!Close && SDL_PollEvent(&SDL_event))
  {
    if (SDL_event.type == SDL_QUIT)
    {
      return false;
    }
    else
    {
      for (auto& receiver : EventReceivers)
      {
        receiver->onEvent(SDL_event);
      }
    }
  }

  auto flags = SDL_GetWindowFlags(Window);
  if ((flags & SDL_WINDOW_INPUT_FOCUS) == false)
    WindowHasFocus = false;
  else
    WindowHasFocus = true;

  return true;
}

//! pause execution temporarily
void CIrrDeviceSDL::yield()
{
  SDL_Delay(500);
}

//! pause execution for a specified time
void CIrrDeviceSDL::sleep(std::uint32_t timeMs)
{
  SDL_Delay(timeMs);
}

//! Returns time elapsed in milliseconds since initialization
std::uint32_t CIrrDeviceSDL::getTime() const
{
  return SDL_GetTicks();
}

//! sets the caption of the window
void CIrrDeviceSDL::setWindowCaption(const std::string& text)
{
  SDL_SetWindowTitle(Window, text.c_str());
}

//! notifies the device that it should close itself
void CIrrDeviceSDL::closeDevice()
{
  Close = true;
}

//! Sets if the window should be resizable in windowed mode.
void CIrrDeviceSDL::setResizable(bool resize)
{
  if (resize != Resizable)
  {
    if (resize)
      SDL_Flags |= SDL_WINDOW_RESIZABLE;
    else
      SDL_Flags &= ~SDL_WINDOW_RESIZABLE;
    Resizable = resize;
  }
}

//! Minimizes window if possible
void CIrrDeviceSDL::minimizeWindow()
{
  SDL_MinimizeWindow(Window);
}

//! Maximize window
void CIrrDeviceSDL::maximizeWindow()
{
  SDL_MaximizeWindow(Window);
}

//! Get the position of this window on screen
glm::ivec2 CIrrDeviceSDL::getWindowPosition()
{
  glm::ivec2 pos;
  SDL_GetWindowPosition(Window, &pos.x, &pos.y);
  return pos;
}

//! Restore original window size
void CIrrDeviceSDL::restoreWindow()
{
  // do nothing
}

//! returns if window is active. if not, nothing need to be drawn
bool CIrrDeviceSDL::isWindowActive() const
{
  return WindowHasFocus;
}

//! returns if window has focus.
bool CIrrDeviceSDL::isWindowFocused() const
{
  return WindowHasFocus;
}

//! returns if window is minimized.
bool CIrrDeviceSDL::isWindowMinimized() const
{
  return WindowMinimized;
}

} // namespace saga

