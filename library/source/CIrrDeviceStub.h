// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_IRR_DEVICE_STUB_H_INCLUDED__
#define __C_IRR_DEVICE_STUB_H_INCLUDED__

#include "IVideoDriver.h"
#include "ISceneManager.h"
#include "SagaDevice.h"
#include "SIrrCreationParameters.h"

#include <vector>
#include <memory>

namespace saga
{
  class IEventReceiver;

  //! Stub for an Irrlicht Device implementation
  class CIrrDeviceStub : public SagaDevice
  {
  public:

    //! constructor
    CIrrDeviceStub(const SIrrlichtCreationParameters& param);

    //! destructor
    virtual ~CIrrDeviceStub();

    //! returns the video driver
    virtual const std::shared_ptr<video::IVideoDriver>& getVideoDriver() const override
    { return VideoDriver; }

    //! returns the scene manager
    const std::shared_ptr<scene::ISceneManager>& getSceneManager() const override
    { return SceneManager; }

    //! Register your event listener, which receives SDL_Event
    void addEventReceiver(IEventReceiver* receiver) override;

    //! Checks if the window is running in fullscreen mode.
    bool isFullscreen() const override;

  protected:
    void createSceneManager();

    std::shared_ptr<video::IVideoDriver> VideoDriver = nullptr;
    std::shared_ptr<scene::ISceneManager> SceneManager = nullptr;

    std::vector<IEventReceiver*> EventReceivers;

    SIrrlichtCreationParameters CreationParams;
    bool Close;
  };

} // namespace saga

#endif

