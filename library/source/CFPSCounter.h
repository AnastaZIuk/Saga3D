// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_FPSCOUNTER_H_INCLUDED__
#define __C_FPSCOUNTER_H_INCLUDED__



namespace saga
{
namespace video
{


class CFPSCounter
{
public:
  CFPSCounter();

  //! returns current fps
  std::int32_t getFPS() const;

  //! returns primitive count
  std::uint32_t getPrimitive() const;

  //! returns average primitive count of last period
  std::uint32_t getPrimitiveAverage() const;

  //! returns accumulated primitive count since start
  std::uint32_t getPrimitiveTotal() const;

  //! to be called every frame
  void registerFrame(std::uint32_t now, std::uint32_t primitive);

private:

  std::int32_t FPS;
  std::uint32_t Primitive;
  std::uint32_t StartTime;

  std::uint32_t FramesCounted;
  std::uint32_t PrimitivesCounted;
  std::uint32_t PrimitiveAverage;
  std::uint32_t PrimitiveTotal;
};


} // namespace video
} // namespace saga


#endif

