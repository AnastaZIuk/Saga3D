// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CIrrDeviceSDL.h"
#include "Saga.h"

namespace saga
{
  //! stub for calling createDeviceEx
  std::unique_ptr<SagaDevice> createDevice(video::E_DRIVER_TYPE driverType,
    const glm::uvec2& windowSize,
    std::uint32_t bits, bool fullscreen,
    bool stencilbuffer, bool vsync)
  {
    SIrrlichtCreationParameters p;
    p.DriverType = driverType;
    p.DeviceType = E_DEVICE_TYPE::SDL;
    p.WindowSize = windowSize;
    p.Bits = bits;
    p.Fullscreen = fullscreen;
    p.Stencilbuffer = stencilbuffer;
    p.Vsync = vsync;

    return createDeviceEx(p);
  }

  std::unique_ptr<SagaDevice> createDeviceEx(const SIrrlichtCreationParameters& params)
  {
    return std::make_unique<CIrrDeviceSDL>(params);
  }

} // namespace saga

