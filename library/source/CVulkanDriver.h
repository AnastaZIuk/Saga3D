#ifndef __C_VIDEO_VULKAN_H_INCLUDED__
#define __C_VIDEO_VULKAN_H_INCLUDED__

#include "enumType.hpp"
#include "CVideoDriver.h"
#include "ISceneNode.h"
#include "SIrrCreationParameters.h"

#ifdef _WIN32
#define VK_USE_PLATFORM_WIN32_KHR
#elif defined __linux__ 
#ifdef __ANDROID__
  #define VK_USE_PLATFORM_ANDROID_KHR
#else
  #define VK_USE_PLATFORM_XLIB_KHR
#endif
#elif defined __APPLE__
  #if TARGET_OS_IPHONE == 1
    #define VK_USE_PLATFORM_IOS_MVK
  #else
    #define VK_USE_PLATFORM_MACOS_MVK
  #endif
#endif

#include <VEZ.h>
#include <glm/vec3.hpp>
#include <vector>

namespace saga
{
class CIrrDeviceSDL;

namespace video
{
  class CVulkanDriver : public CVideoDriver
  {
  public:
    //! Constructor
    CVulkanDriver(const SIrrlichtCreationParameters& params, CIrrDeviceSDL& device);

    //! Destructor
    virtual ~CVulkanDriver();

    virtual const std::string& getVendorName() const override { return VendorName; }

    bool initDriver();

    virtual void begin() override;
    virtual void beginPass(RenderPassHandle pass) override;
    virtual void draw() override;
    virtual void endPass() override;
    virtual void present(TextureHandle texture = NULL_GPU_RESOURCE_HANDLE) override;
    virtual void submit() override;
    virtual void end() override;

    virtual std::uint64_t getAllocatedMemory() const override;

    virtual TextureHandle createTexture(STexture&& texture) override;
    virtual TextureHandle createTexture(const std::string& path) override;
    virtual TextureHandle createTexture(unsigned char* data, std::size_t size) override;
    virtual void destroyTexture(const TextureHandle texture) override;
    virtual void bindTexture(TextureHandle texture, const int binding) override;

    virtual ShaderHandle createResource(SShader&& shader) override;
    virtual void destroyShader(const ShaderHandle shader) override;
    virtual ShaderUniformHandle createResource(SShaderUniform&& uniform) override;
    virtual void destroyShaderUniform(const ShaderUniformHandle uniform) override;
    virtual ShaderBufferHandle createResource(SShaderBuffer&& buffer) override;
    virtual void destroyShaderBuffer(const ShaderBufferHandle buffer) override;
    virtual PipelineHandle createResource(SPipeline&& pipeline) override;
    virtual void destroyPipeline(const PipelineHandle p) override;
    virtual RenderPassHandle createResource(SRenderPass&& pass) override;
    virtual void destroyRenderPass(const RenderPassHandle pass) override;
    void createFramebuffer(const RenderPassHandle pass);
    void destroyFrameBuffer(const RenderPassHandle pass);

    virtual void bindComputePipeline(const PipelineHandle& compute) override;
    virtual void dispatchComputePipeline(std::uint32_t x, std::uint32_t y, uint32_t z) override;

    virtual void updateShaderUniform(const ShaderUniformHandle uniform, const void* data) override;
    virtual void bindShaderUniform(const ShaderUniformHandle uniform, const int binding) override;
    virtual void updatePushConstant(const PushConstantHandle con, const void* data, const size_t offset = 0, const size_t size = 0) override;
    virtual void updateShaderBuffer(const ShaderBufferHandle buffer, const void* data, const size_t offset = 0, const size_t size = 0) override;
    virtual void bindShaderBuffer(const ShaderBufferHandle buffer, const int binding) override;

    virtual IndirectBufferHandle createResource(SIndirectBuffer&& buffer) override;
    virtual void destroyIndirectBuffer(const IndirectBufferHandle buffer) override;
    virtual IndexedIndirectBufferHandle createResource(SIndexedIndirectBuffer&& buffer) override;
    virtual void destroyIndexedIndirectBuffer(const IndexedIndirectBufferHandle buffer) override;

    virtual void* mapBuffer(const ShaderBufferHandle buffer,
      const std::size_t offset = 0, const std::size_t size = 0) const override;
    virtual void unmapBuffer(const ShaderBufferHandle buffer) const override;

    virtual void createPipelineBuffer(const scene::IMeshSceneNode& node) override;
    virtual void destroyPipelineBuffer(const scene::ISceneNode& node, bool destroyAll = false) override;

    virtual std::uint32_t getWidth() const override;
    virtual std::uint32_t getHeight() const override;

    virtual void copyTexture(TextureHandle srcTex, TextureHandle dstTex,
      const glm::ivec2& srcOffset, const glm::ivec2& dstOffset, const glm::ivec2& size) override;

    virtual void blitTexture(TextureHandle srcTex, TextureHandle dstTex,
      const glm::ivec2& srcOffset = {}, const glm::ivec2& dstOffset = {}) override;

    VkImage getVkTexture(const TextureHandle t) const;

  private:
    VkShaderModule createShader(ShaderHandle shader, VkShaderStageFlagBits stage);
    void createDefaultFramebuffer();
    void createCommandBuffer();
    void createTexture(TextureHandle texture);

  private:
    VkQueue GraphicsQueue = VK_NULL_HANDLE;
    VkCommandBuffer CommandBuffer = VK_NULL_HANDLE;
    VkSemaphore Semaphore = VK_NULL_HANDLE;

  private:
    struct VkPipelineBuffer {
      VkBuffer VertexBuffer = VK_NULL_HANDLE;
      VkBuffer IndexBuffer = VK_NULL_HANDLE;
    };

    using PipelineBuffers = std::unordered_map<PipelineHandle, VkPipelineBuffer>;
    std::unordered_map<ID, PipelineBuffers> VkPipelineBuffers;

   private:
    std::string Name;
    std::string VendorName;

    SIrrlichtCreationParameters Params;
    CIrrDeviceSDL& Device;
    E_DEVICE_TYPE DeviceType;

    int PhysicalDeviceIndex = 0;
    #ifndef NDEBUG
    bool EnableValidationLayers = true;
    #else
    bool EnableValidationLayers = false;
    #endif
    bool ManageFramebuffer = true;
    VkSampleCountFlagBits SampleCountFlag = VK_SAMPLE_COUNT_1_BIT;
    std::vector<const char*> DeviceExtensions;
    VkPhysicalDeviceFeatures DeviceFeatures = {};
    VkInstance Instance = VK_NULL_HANDLE;
    VkPhysicalDevice PhysicalDevice = VK_NULL_HANDLE;
    VkSurfaceKHR Surface = VK_NULL_HANDLE;
    VkDevice VKDevice = VK_NULL_HANDLE;
    VezSwapchain Swapchain = VK_NULL_HANDLE;
    std::vector<VkImage> SwapchainImages;
    VkPhysicalDeviceMemoryProperties MemoryProperties = {};
    mutable VkPhysicalDeviceMemoryProperties2 MemoryProperties2 = {};
    mutable VkPhysicalDeviceMemoryBudgetPropertiesEXT MemoryBudgetProperties = {};
    std::vector<int> MemoryHeapIndices;

    struct VulkanTexture {
      VkImage Image;
      VkImageView ImageView;
      VkSampler Sampler;
    };

    std::unordered_map<TextureHandle, VulkanTexture> VkTextures;
    std::unordered_map<ShaderHandle, std::vector<VkShaderModule>> VkShaderModules;
    std::unordered_map<ShaderUniformHandle, VkBuffer> VkShaderUniforms;
    std::unordered_map<ShaderBufferHandle, VkBuffer> VkShaderBuffers;
    std::unordered_map<IndirectBufferHandle, VkBuffer> VkIndirectBuffers;
    std::unordered_map<IndexedIndirectBufferHandle, VkBuffer> VkIndexedIndirectBuffers;

    struct VulkanPipeline {
      VezPipeline Handle;
      VkVertexInputBindingDescription VertexInputBinding;
      VezVertexInputFormat VertexInputFormat;
    };
    std::unordered_map<PipelineHandle, VulkanPipeline> VkPipelines;
    std::unordered_map<RenderPassHandle, VezFramebuffer> VkFramebuffers;

    struct VulkanFramebuffer {
      VkImage colorImage = VK_NULL_HANDLE;
      VkImageView colorImageView = VK_NULL_HANDLE;
      VkImage depthStencilImage = VK_NULL_HANDLE;
      VkImageView depthStencilImageView = VK_NULL_HANDLE;
      VezFramebuffer handle = VK_NULL_HANDLE;
    };
    VulkanFramebuffer DefaultFramebuffer = {};
  };

} // namespace video
} // namespace saga

#endif // __C_VIDEO_VULKAN_H_INCLUDED__
